# BitSharp

BitSharp intends to be a fully validating Bitcoin node written in C#. This project is currently being prototyped and should be considered pre-pre-alpha.

Please refer to the BitSharp wiki for all information.

https://github.com/pmlyon/BitSharp/wiki

## License

BitSharp is free and unencumbered software released into the public domain.

See https://github.com/pmlyon/BitSharp/blob/master/LICENSE
